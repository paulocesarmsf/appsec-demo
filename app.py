import subprocess

from flask import Flask, request

app = Flask(__name__)


@app.route('/', methods=['GET'])
def health():
    return 'ok'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8002) # nosemgrep
