import json
from lxml import html


class ParserOwaspDependencyCheck(object):

    def parse_html(self):
        with open('../tests/dependency-check-report.html') as html_file:
            tree = html.fromstring(html_file.read())

            scan_information = self.parse_scan_information(tree)
            vulns = self.parse_vulns(tree)

            output = {
                'scan_information': scan_information,
                'vulnerabilities': vulns
            }
            print(output)

    def parse_scan_information(self, tree):
        return {
            'vulnerable_count': tree.xpath('//*[@id="vulnerableCount"]')[0].text,
            'dependency_scanned':tree.xpath('//li[contains(i,"Dependencies Scanned")]/text()')[0].replace(':',
                                                                                                          '').strip(),
            'vulnerabilities_found': tree.xpath('//li[contains(i, "Vulnerabilities Found")]/text()')[0].replace(':',
                                                                                                           '').strip()
        }

    def parse_vulns(self, tree):
        vulns = []
        tree_vulns = tree.xpath('//*[@id="summaryTable"]/tbody/tr[not(contains(@class, "notvulnerable")) '
                                'and contains(@class, "vulnerable")]')

        for tree_vuln in tree_vulns:
            vuln = {'lib': self.get_xpath(tree_vuln, 'td[1]/a/text()')[0],
                    'pkg': self.get_xpath(tree_vuln, 'td[3]/a/text()')[0],
                    'severity': self.get_xpath(tree_vuln, 'td[4]/text()')[0],
                    'confidence': self.get_xpath(tree_vuln, 'td[7]/text()')[0]}
            vuln.update({'info': self.get_dependence_info(vuln.get('lib'))})
            vulns.append(vuln)

        return vulns

    def get_xpath(self, tree, xpath):
        try:
            return tree.xpath(xpath)
        except IndexError:
            return ['N/A']

    def get_dependence_info(self, dependence_name):
        with open('../tests/dependency-check-report_v2.json') as obj_json:
            obj = json.load(obj_json)
            for dependence in obj.get('dependencies'):
                if dependence.get('fileName') == dependence_name:
                    return dependence


ParserOwaspDependencyCheck().parse_html()
